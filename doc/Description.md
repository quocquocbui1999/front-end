# Description

This project includes a script for downloading and compiling the appropriate components to provide a document environment.

Documentation environment consists of:
* eclipse install instructions for environment IDE. TODO
* Doxygen with support for system verilog
    See: http://www.doxygen.org/
* PlantUML installation and configuration with doxygen for state mahcine diagrams and other useful information.
    See: http://plantuml.com/
* Wavedrom installation and configuration scripts with doxygen for timing waveform and 'logic-gate' documentation.
    See: http://wavedrom.com/tutorial.html
 
# Acronymns

# Features
* Auto generated engineering documentation with info from Doxygen.
* Examples are the std-modules that can be found in other directories.

# References
    Doxygen: http://www.doxygen.org/
    Plantuml: http://plantuml.com/
    Wavedrom: http://wavedrom.com/tutorial.html
 
# How to embed Wavedrom timing in doxygen:
    \htmlonly
    <script type="WaveDrom">
    {signal:
     [
        ['Template_NormalCase',
          {name: 'fault'  , wave: '1......0...|=....0' ,data:['value <> threshold']},
          {name: 'alarm'  , wave: '0..........|=....0' ,data:['fault & ramped']},
          {name: 'ramping', wave: '0...1..0...|=....0' ,data:['active & !ramped']},
          {name: 'ramped' , wave: '0......1...|=....0' ,data:['ramping | (!fault & condition)']},
          {name: 'active' , wave: '0...1......|=....0' ,data:['all_prev_ramped']}
        ],
       {},
       {},
       {},
        ['Template_Timeout',
         {name: 'fault'  , wave: '1..........0..|=....0' ,data:['value <> threshold'], node:'...........f'},
          {name: 'alarm'  , wave: '0.........1..0|=....0' ,data:['fault & ramped'], node:'..........a..c'},
          {name: 'ramping', wave: '0...1...0.....|=....0' ,data:['active & !ramped'], node:'........t'},
         {name: 'ramped' , wave: '0.......1.....|=....0' ,data:['ramping | (!fault & condition)'], node:'....s'},
          {name: 'active' , wave: '0...1.........|=....0' ,data:['all_prev_ramped']}
        ],
     ],
     edge: ['t~>a Rise-time', 'f~>c Fall-time', 's~>t Timeout'],
    }
    </script>

    \endhtmlonly 
# Block Diagram
See Doxygen generated info.
# Port Description
See Doxygen generated info.
# Register/AddressMap
N/A
# Operation
 See assertions for expected operation.
 